// gs = Google Script
var gsURL = "https://script.google.com/macros/s/AKfycbwqG5dsDQO32T4lulP_eJkQRXe50YO5EalQcrpr2v5a6HTgq_8/exec";  // Deployed version


function runSetup() {
  var name = $("#classname").val();
  
  var email = $("#email").val();
  
  if( email == null || !(/\S+@\S+\.\S+/.test(email)) ) {
		alert( "You must provide a valid email address." );
		return;
  }
  
//  var sendEmail = $("#autoemail").prop("checked");
  
  var gsDebug = $("#debugmode").prop("checked");
  var gsLive = !($("#dummymode").prop("checked"));
  
  $("#start").hide();
  $("#loading").show();

  var requestURL = gsURL + "?&name=" + escape(name) + "&email=" + escape(email) /* + (sendEmail ? "&sendemail=false" : "") */ + (gsDebug ? "&debug=true" : "") + (gsLive ? "&live=true" : "");

  console.log( "Query URL: " + requestURL );
  window.location = requestURL;
}

function reset() {
  $("#loading").hide();
  $("#start").show();
};

$(window).on("pageshow", reset);


/* jQuery code to slide in/out FAQ answers by clicking question title */
function doSlide( node ) {
  /* Show/hide all elements between clicked element and the next <li> element */
  $(node).nextUntil(".question").slideToggle();
}

var qs = $("#faqs > .question");

/* Set onclick handler */
qs.click(function(){doSlide(this);})

/* Hide answers initially */
qs.nextUntil(".question").hide();

$("#clicktoshow").show();

function showdebug() {
  document.getElementById("showdebug").style.display = "none";
  document.getElementById("debug").style.display = "";
}

