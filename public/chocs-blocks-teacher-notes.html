<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <title>Chocs and Blocks Activity - notes for teachers</title>

    <link href="css/styles-2.css" rel="stylesheet">
    <link rel="stylesheet" href="css/lightbox.min.css">

  <style>
    @import url(https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,300italic,100italic,100,500italic,400italic);
  </style>
<style>
.hidden {
  display: none;
}
</style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

  </head>
  <body>
  <div class="page-inner">
    <div class="page-header">
      <header>
       <a class="page-header-logo" href="https://www.unimelb.edu.au">
        <svg width="140" height="140" viewBox="0 0 140 140" aria-labelledby="aria-uom-title" role="img">
          <title id="aria-uom-title">The University of Melbourne Logo</title>
          <image xlink:href="https://uom-design-system.s3.amazonaws.com/shared/assets/logo.svg" src="https://uom-design-system.s3.amazonaws.com/shared/assets/logo.png" alt="The University of Melbourne Logo" width="140" height="140" preserveAspectRatio="xMaxYMin meet"></image>
        </svg>
      </a>

      <div class="page-header-navigation">
        <div class="page-local-history">
          
          </div>
        </div> <!-- /.page-header-navigation -->
      </header><!-- /header -->
      
    </div><!-- /.page-header -->

  <!-- begin main content -->

  <div class="main">
      <header>
        <h1 class="center">Chocs and Blocks</h1>
      </header><!-- /header -->
      <div>
      <h2>Notes for teachers</h2>
      <p><i>Chocs and Blocks</i> is an in-class sampling activity designed to provide an experiential foundation upon which students can build understandings of sampling and sampling bias, sampling variability and estimation, sampling distributions and other key statistical concepts.  These notes are intended for teachers (including lecturers, tutors or secondary school teachers) to help you get started with the Chocs and Blocks activity.</p>
      <div class="reflection">Note: Paragraphs in this style are additional comments and reflections which the reader might skip on a first read and return to later.</div>
      <h3>Requirements</h3>
      <p>To run the <i>Chocs and Blocks</i> activity, you will need:
      <ul>
        <li>One or more trays of chocolate pieces, about 100 pieces per tray;</li>
        <li>One or more teachers or assistants to facilitate the activity;
          <div class="reflection">It is ideal to have someone leading the discussion and focusing the activity and a second person managing the technology. For large cohorts, such as in a lecture theatre, having additional tutors for the ‘finding a sampling strategy’ part of the activity helps to keep the conversation focused.</div>
        </li>
        <li>A classroom with internet access for students (either fixed PCs as in a computer lab or Wifi access so students can use their laptops/tablets/etc);</li>
        <li>One or more computer projectors to display web pages to the class.
        <div class="reflection">For a small class, eg. 30 or less, you can get by without a projector. See comments under ‘<a href="#technical">Technical setup</a>’ below.</div></li>
      </ul>
      </p>
      <h3>Overview of the activity</h3>
      <p>
      Students are presented with a tray of 100 chocolate pieces. The pieces are irregularly shaped and vary in size. The task for students is to come up with an estimate of the <i>average weight of a piece of chocolate</i> on the tray.  The most accurate estimate will be rewarded with first pick of a piece of chocolate from the tray.
      <div class="reflection">We make sure there is one piece that is larger than the rest, conspicuously placed on the tray.</div>
      <div style="text-align: center;">
       <img src="img/chocs.jpg" style="width: 320px; height: 240px;" data-jslghtbx>
       <br>
       <div class="caption">Figure  1: Tray of chocolate pieces of irregular shape and varying sizes.</div>
      </div>
      Initially, students aren’t given any further guidance than this – except that they are allowed to ask questions of the teachers/assistants, who may or may not be able to answer.  The teachers can provide only the following information, if students have <b>asked the right question</b> to elicit the information:
      <ul>
        <li>There are 100 pieces of chocolate on the tray (they are told this at the start);</li>
        <li>The weight of each individual piece of chocolate is known, and students can find out the weights of individual pieces in the next stage of the activity;</li>
        <li>Each student can find out the weight of 10 pieces  on the tray, but they must specify which 10 pieces.
          <div class="reflection">Choice of sample size and whether or not everyone should use the same sample size could be a discussion point at this stage. But note that if all students use the same sample size then the data easily lends itself to exploration of the Central Limit Theorem later on.</div>
        </li>
      </ul>
      The teachers evade other questions, such as “what is the total weight of all pieces?”, with replies such as “I don’t remember!”. (Students are not allowed to touch the chocolate at this stage “for hygiene reasons” but can be promised chocolate to eat at the end of class.)
      </p>
      <div class="reflection">An important (and challenging!) part of this task is leanring how to ask the right questions to get the information that you need. Students often are very challenged by this!</div>
      <p>
      Once students have discovered that they will be given the chance to find out weights of specific pieces, their task is to come up with a strategy for which pieces to choose.  When most of the class has reached this stage, the teacher directs students to open the <i>Chocs and Blocks</i> website on their computer, tablet or phone. The teacher tells them:
      </p>
      <blockquote>
      “The blocks on screen are a virtual representation of the pieces of chocolate. Your task now is to select 10 pieces, whose weights you will be given.”
      </blockquote>
      <p>
      Students select their 10 blocks using the website, and the website calculates the individual weights and mean weight of the blocks they’ve chosen. Students submit their mean weight to the teacher, for instance by students calling out their value one-by-one and the teacher recording them in a spreadsheet, or by students submitting their values into an online spreadsheet themselves via an online form.
      <div style="text-align: center; margin-top: 1em;">
       <img src="img/cb-screenshot.png" style="margin-right: 0.5em; width: 278px; height: 361px; border: 1px solid grey;" data-jslghtbx>
       <br>
       <div class="caption">Figure  2: Chocs and Blocks website</div>
      </div>
      Once the students have recorded their data, the teacher can download the data into a statistics package, eg. Minitab, and produce graphical displays of the sample means (eg. dotplot (<a href="#fig3">Figure 3</a>), stem-and-leaf plot) and lead a discussion of the shape &amp; features of the distribution.  The teacher could focus on some particular data points, eg outliers, or those around the center, and ask the corresponding students to describe the strategy that they used to select their blocks. This is also a good time to discuss assumptions that students may have made (e.g. about the distribution of weights or the relationship between block numbers and weights), especially if they have used a systematic sampling method (e.g. every 10th piece).
      <div class="reflection">Actually, the distribution of block weights is positively skewed, and block numbers are negatively correlated with weights (smaller numbered blocks tend to be larger)!
      This will result in some sampling strategies - eg, taking block numbers 1-10 - giving biased estimates.</div>
      <div style="text-align: center; margin-top: 1em;" id="fig3">
       <img src="img/dotplot.png" style="border: 1px solid grey;" data-jslghtbx>
       <br>
       <div class="caption">Figure  3: Example dotplot of means from choice samples</div>
      </div>
      After discussing the distribution and some strategies, the teacher can reveal the true mean, which is 33.5, and those who got closest with their choice estimates are asked to explain their strategy and then rewarded with chocolate.
      <div class="reflection">Students consistently overestimate the mean by around 10 units - and are often surprised that they were so wrong!</div>
      <p>The next step  is to direct students to click on a hidden link on the <i>Chocs and Blocks</i> page (the link labelled ‘chocs &amp; blocks’ at the bottom-right of the page - see <a href="#fig4">Figure 4</a> below), which takes them to ‘random sample’ mode. In this mode, the computer will take a random sample of 10 blocks.  Students again submit their sample means to the teacher.
      </p>
      <div class="reflection">This stage of the activity tends to be very quick.</div>      
      <div style="text-align: center; margin-top: 1em;" id="fig4">
       <img src="img/secretlink.png" style="border: 1px solid grey; width: 283px; height: 208px;" data-jslghtbx>
       <br>
       <div class="caption">Figure  4: Clicking the circled link will switch between ‘choice sample’ and ‘random sample’ modes.</div>
      </div>
      The teacher again downloads the data and produces graphical displays of the distribution of sample means.  This can be compared to the distribution  obtained earlier from the choice samples (<a href="#fig5">Figure 5</a>).
      <div class="reflection">The discussion here can be a pre-cursor to more formal investigation of the Central Limit Theorem later in the course. It's good to assure students that historically students over-estimate the mean by as much as 10 units! This could open the way for “why do you think this is so?”</div>
      <div style="text-align: center; margin-top: 1em;" id="fig5">
       <img src="img/dotplots2.png" style="border: 1px solid grey;" data-jslghtbx>
       <br>
       <div class="caption">Figure  5: Example dotplots of sample means from choice samples and random samples.</div>
      </div>
      Finally, everyone can take some chocolate on the way out!
      </p>
      <h3>Pedagogical considerations</h3>
      <p>
      It can take students a long time to discover that they can ask the weights of individual pieces. Some hints might be needed periodically to help them reach this point.
      </p>
      <p>
      Common strategies for choosing blocks include “choose 5 large and 5 small”, “take every 10th block, ie blocks 1, 11, 21, 31,…”, “take all blocks on the bottom row”.  These can lead to some discussion:  Is the distribution of block weights/sizes symmetric? (No!) Is there any correlation between block numbers and sizes? (Yes! Larger blocks tend to have lower numbers.)  How might these affect the resulting estimates?
      </p>
      <p>
      The true mean of the block weights is 33.5.  The choice samples always tend to overestimate this. (This has been true every time the activity has been run for over 15 years.)  Students are often surprised to see how far off they, and much of the rest of the class, were in their estimates.  This is a powerful motivator for the need to study sampling methods and the value of random sampling.
      </p>
      <p>
       A spreadsheet of all block numbers and weights <a href="block-weights.csv">can be downloaded here</a>.
      </p>
      <h3 id="practical">Practical considerations</h3>
      <p>For large classes, you will likely need more than one tray of chocolate and several teaching staff to answer questions and carry around the trays.  For a lab class of ∼40 students we use 2 tutors; for a lecture class of ∼200 we have 1 lecturer and 4-5 tutors.</p>
      <p>We suggest not using a familiar supermarket brand of chocolate, eg Cadbury, as students might focus on the weight of a whole block which they could easily Google.  We use sheets of chocolate from <a href="http://www.haighschocolates.com.au/">Haigh's</a>, which are easy to break into irregular shapes and hard to Google for. Haigh’s chocolate comes in sheets so it has uniform density. Students are then looking for ‘representativeness  of the sample’ based on shape and size of pieces only.  This helps to keep things simple.</p>
      <h3 id="technical">Technical setup</h3>
      <p>For small classes (about 30 or less), it's fine to just have students call out their sample means one-by-one and record the data manually on a whiteboard or Excel spreadsheet.  For large classes, we recommend that students submit their sample means directly into an online spreadsheet via an online form. It is possible to partly automate the submission process so that students can submit directly from the Chocs and Blocks page into a Google Sheet. If you are interested in using this, contact Anthony Morphett <a href="mailto:a.morphett@unimelb.edu.au">a.morphett@unimelb.edu.au</a>
      </p>
      <p>
      We suggest that you put the <i>Chocs and Blocks</i> web address <a href="https://go.unimelb.edu.au/u4b8">https://go.unimelb.edu.au/u4b8</a> and the QR code below on a Powerpoint slide and display it to the class when you are ready for them to access the <i>Chocs and Blocks</i> website. You might also write the web address on a whiteboard.
      <br>
       <img src="img/qrcode.png" width="151" height="150"/>
      </p>
      <h3>Suggested running sheet</h3>
      <p>
      Here is a suggested running sheet for Chocs and Blocks in a 50-minute class.
      <br>
      <table width="100%" border="1" cellspacing="1" cellpadding="0" style="border-collapse:collapse;">
  <tbody>
    <tr>
      <td>
        <p><b>When</b></p>
      </td>

      <td>
        <p><b>What’s happening</b></p>
      </td>

      <td>
        <p><b>What’s shown on projectors</b></p>
      </td>
    </tr>

    <tr>
      <td>
        <p>0-2 minutes </p>
      </td>

      <td>
        <p>Teacher introduces activity </p>
      </td>

      <td rowspan="2"><p align="center">Nothing</p></td>
    </tr>

    <tr>
      <td>
        <p>2-20 minutes </p>
      </td>

      <td>
        <p>Teachers/assistants carry around chocolate trays to show students</p>

        <p>Students ask questions and try to formulate strategy to estimate mean</p>

        <p>Teachers/assistants prompt students, give hints if necessary </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>20 minutes </p>
      </td>

      <td>
        <p>Teacher directs students to load Chocs and Blocks web page </p>
      </td>

      <td rowspan="2">
        <p align="center">Chocs and Blocks web address and QR code</p>

        <p align="center">Chocs and Blocks web page</p>

        <p align="center">Spreadsheet of choice sample results </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>20-30 minutes </p>
      </td>

      <td>
        <p>Students choose blocks and submit choice samples </p>
      </td>

    </tr>

    <tr>
      <td>
        <p>30-40 minutes </p>
      </td>

      <td>
        <p>Teacher analyses choice sample means and leads discussion of strategies, interrogation of assumptions</p>
        <p>
        Reveal true mean; reward best estimate(s) with chocolate
        </p>
      </td>

      <td>
        <p align="center">Statistical software eg. Minitab</p>

        <p align="center">Spreadsheet of choice sample results </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>40 minutes </p>
      </td>

      <td>
        <p>Teacher shows students how to switch to ‘random sample’ mode </p>
      </td>

      <td>
        <p align="center">Chocs and Blocks web page </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>40-42 minutes </p>
      </td>

      <td>
        <p>Students generate and submit random samples </p>
      </td>

      <td>
        <p align="center">Spreadsheet of random sample results </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>42-50 minutes </p>
      </td>

      <td>
        <p>Teacher analyses random sample means, compares with choice samples, discussion </p>
      </td>

      <td>
        <p align="center">Statistical software eg. Minitab </p>
      </td>
    </tr>

    <tr>
      <td>
        <p>50 minutes </p>
      </td>

      <td>
        <p align="left">Distribute chocolate to everyone as they leave </p>
      </td>

      <td></td>
    </tr>
  </tbody>
</table>
      
      </p>
      <h3>Acknowledgements</h3>
      <p><i>Chocs and Blocks</i> is based on an earlier activity by Professor Nye John and Dr David Whittaker from the University of Waikato at the end of the last century.</p>
      <p>The <i><a href="activity/index.html">Chocs and Blocks</a></i> webpage was designed by Sharon Gunn and <a href="mailto:a.morphett@unimelb.edu.au">Anthony Morphett</a> and developed by <a href="https://le.unimelb.edu.au/">Learning Environments</a> and Anthony Morphett, with financial support from the <a href="https://ms.unimelb.edu.au/">School of Mathematics and Statistics</a>. An earlier Chocs and Blocks website was developed by Russell Jenkins.  The Chocs and Blocks setup program was developed by Anthony Morphett. This documentation was written by Sharon Gunn and Anthony Morphett.</p>
      <h3>Further references</h3>
      <p>
      Sharon Gunn. (2011). Towards an Understanding of Statistical Task Design. <i><a href="http://www.math.canterbury.ac.nz/~j.hannah/Delta11/VolcanicDelta2011Proceedings.pdf">Proceedings of Volcanic Delta 2011: The Eighth Southern Hemisphere Conference on Teaching and Learning Undergraduate Mathematics and Statistics, Rotorua, New Zealand, 27 November - 2 December 2011</a></i>, p.120 - 128
      </p>
      <p>
      Sharon Gunn and Anthony Morphett. (2017). An interactive statistical sampling activity with chocolate. <i><a href="https://mav.vic.edu.au/files/2017/MAV17-Conference/Conference_Proceedings_011217.pdf">2017 Mathematical Association of Victoria Annual Conference proceedings</a></i>, p.25 - 30
      </p>


    </div>
<!---       <div class="page-footer">
        <footer>
          <a class="unimelb-logo" href="https://unimelb.edu.au">
            <svg width="300" height="100" viewBox="0 0 300 100" aria-labelledby="aria-uom-title" role="img">
              <image xlink:href="https://uom-design-system.s3.amazonaws.com/shared/assets/lockup.svg" src="https://uom-design-system.s3.amazonaws.com/shared/assets/lockup.png" alt="The University of Melbourne Logo" width="300" height="100" preserveAspectRatio="xMaxYMin meet"></image>
            </svg>
          </a>
!-->
<!--
          <ul class="page-footer-section nav">
            <li><a href="http://safety.unimelb.edu.au/about/contacts/emergency.html">Emergency Information</a></li>
            <li><a href="http://www.unimelb.edu.au/disclaimer/">Disclaimer &amp; Copyright</a></li>
            <li><a href="http://www.unimelb.edu.au/accessibility/index.html">Accessibility</a></li>
            <li><a href="http://www.unimelb.edu.au/disclaimer/privacy.html">Privacy</a></li>
          </ul>
!-->
<!--        </footer>
      </div>
!-->

  </div><!-- /.main -->
</div>

  </body>
 <script src="js/lightbox.min.js" type="text/javascript"></script>
 <script>
 var lightbox = new Lightbox();
 var lightBoxOptions = {
   nextOnClick: false
 };
 lightbox.load(lightBoxOptions);
 </script> 
</html>
