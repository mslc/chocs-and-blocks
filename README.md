# Chocs and Blocks

This repository hosts the Chocs and Blocks interactive class activity.

It is deployed directly from this repository to the web at https://mslc.pages.gitlab.unimelb.edu.au/chocs-and-blocks/ via Gitlab Pages.

Changes to files in the public folder will automatically be reflected on the live site within seconds.

This repository contains the Chocs and Blocks web app, documentation, and the client-side part of the Chocs and Blocks setup program.

The Chocs and Blocks setup program also has a server-side Google Script component.

It is stored in Google at https://script.google.com/home/projects/1K8JgxN2qLoygUmQ4JJk1MAv7YQ3x03H2fF4GLEyrDfIiGWLWGvXDUpFj/edit and is owned by the MSLC Google account mslcunimelb@gmail.com